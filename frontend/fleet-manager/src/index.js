import React from 'react';
import ReactDOM from 'react-dom';
import FleetManager from './FleetManager';

ReactDOM.render(<FleetManager />, document.getElementById('fleet-manager'));
