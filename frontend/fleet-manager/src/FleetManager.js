import React, {useState, useEffect} from 'react';
import poiType from './types/poiType';
import './FleetManager.css'

const defaultLatitude = -34.397;
const defaultLongitude = 150.644;
const defaultRadius = 50000;

const radiusOptions =  [
  {
    description: "Select radius",
    value: defaultRadius,
  },
  {
    description: "500 meters",
    value: 500,
  },
  {
    description: "1 Km",
    value: 1000,
  },
  {
    description: "1500 meters",
    value: 1500,
  },
  {
    description: "2 Km",
    value: 2000,
  },
];

const renderOption = (value, description) => {
  return (
    <option key={value} value={value}>{description}</option>
  );
};

const renderPoiOptions = () => {
  let options = [];
  Object.keys(poiType).forEach((key) => {
    options.push(renderOption(key, poiType[key].description));
  });
  return options;
}

const getGoogleLatLng = (latitude, longitude) => new window.google.maps.LatLng(latitude, longitude);

export const getMarkerImgFromGoogleType = (googleType) => {
  let image;
  Object.keys(poiType).forEach((key) => {
    if (key !== 'VIEW_ALL' && poiType[key].googleType.includes(googleType)) {
      image = poiType[key].image;
    };
  });
  return image;
};

let arrow = null;
let infoWindow = null;

const clearArrow = () => {
  if (arrow !== null) {
    arrow.setMap(null);
  }
};

const clearInfoWindow = () => {
  if (infoWindow !== null) {
    infoWindow.close();
    infoWindow = null;
  }
};

const FleetManager = () => {
  const [licence, setLicence] = useState("");
  const [poi, setPoi] = useState('VIEW_ALL');
  const [radius, setRadius] = useState(defaultRadius);
  const [map, setMap] = useState(null);
  const [markers, setMarkers] = useState([]);

  const recenterMap = (latLng) => {
    map.setCenter(latLng);
  };

  const getPoiClickEvent = (marker, truckLocation, latitude, longitude) => {
        clearInfoWindow();
        const truckGoogleLatLng = getGoogleLatLng(truckLocation.latitude, truckLocation.longitude);
        const distance = window.google.maps.geometry.spherical.computeDistanceBetween(truckGoogleLatLng, marker.getPosition());
        const contentString = `<div>Distance: ${Math.floor(distance)}m</div>`;
        infoWindow = new window.google.maps.InfoWindow({
          content: contentString
        });
        infoWindow.open(map, marker);
        clearArrow();
        const polyline = new window.google.maps.Polyline({
          path: [{lat: truckLocation.latitude, lng: truckLocation.longitude}, {lat: latitude, lng: longitude}],
          map,
          icons: [{
            icon: {path: window.google.maps.SymbolPath.FORWARD_CLOSED_ARROW},
            offset: '100%'
          }],
        });
        arrow = polyline;
  };

  const setMarker = (latitude, longitude, imagePath, isRoutePath = false, isPoi = false, truckLocation = null) => {
    const size = isRoutePath ? new window.google.maps.Size(15, 15) : new window.google.maps.Size(32, 32);
    const imageAttributes = {
      url: imagePath,
      scaledSize: size,
    };
    const marker = new window.google.maps.Marker({
      position: {lat: latitude, lng: longitude},
      icon: imageAttributes,
    });
    if (isPoi) {
      marker.addListener('click', () => { getPoiClickEvent(marker, truckLocation, latitude, longitude); });
    }
    marker.setMap(map);
    markers.push(marker);
    setMarkers(markers);
  };

  const setCurrentPosition = (location) => {
    const image = `${process.env.PUBLIC_URL}/icn-current-location.png`;
    setMarker(location.latitude, location.longitude, image);
    map.setZoom(15);
  };

  const setFirstLocation = (location) => {
    const image = `${process.env.PUBLIC_URL}/icn-first-location.png`;
    setMarker(location.latitude, location.longitude, image, true);
  };

  const setPathMarkers = (locations) => {
    const image = `${process.env.PUBLIC_URL}/icn-path.png`;
    locations.forEach((location, index) => {
      if (index !== 0 && index !== locations.length - 1) {
        setMarker(location.latitude, location.longitude, image, true);
      }
    });
  };

  const clearMarkers = () => {
    markers.forEach(marker => {
      marker.setMap(null);
    });
    setMarkers([]);
  };

  const setRouteMarkers = (locations) => {
    const currentPosition = locations[0];
    setCurrentPosition(currentPosition);
    if (locations.length > 1) {
      setFirstLocation(locations[locations.length - 1]);
    }
    if (locations.length > 2) {
      setPathMarkers(locations);
    }
  };

  const setPoiMarkers = (results, status, pagination, truckLocation, googleType) => {
    if (status === window.google.maps.places.PlacesServiceStatus.OK) {
      results.forEach(place => {
        if (radius !== undefined && radius !== null && Number(radius) !== defaultRadius) {
          const distance = window.google.maps.geometry.spherical.computeDistanceBetween(getGoogleLatLng(truckLocation.latitude, truckLocation.longitude), place.geometry.location);
          if (distance > radius) {
            return;
          }
        }
        const image = getMarkerImgFromGoogleType(googleType);
        if (image !== undefined) {
          setMarker(place.geometry.location.lat(), place.geometry.location.lng(), image, false, true, truckLocation);
        }
      });
      if (pagination.hasNextPage) {
        pagination.nextPage();
      }
    }
  };

  const getPoi = (latLng, truckLocation) => {
    poiType[poi].googleType.forEach(type => {
      const request = {
        location: latLng,
        rankBy: window.google.maps.places.RankBy.DISTANCE,
        type,
      };
      const service = new window.google.maps.places.PlacesService(map);
      service.nearbySearch(request, (results, status, pagination) => {setPoiMarkers(results, status, pagination, truckLocation, type)});
    });
  }

  const clear = () => {
    clearMarkers();
    clearArrow();
  };

  const updateMap = (locations) => {
    clear();
    if (locations.length > 0) {
      const truckLocation = locations[0];
      const googleLatLng = getGoogleLatLng(truckLocation.latitude, truckLocation.longitude)
      recenterMap(googleLatLng);
      setRouteMarkers(locations);
      getPoi(googleLatLng, truckLocation);
    }
  };

  const getData = () => {
    if (licence === undefined || licence === null || licence.length === 0) {
      clear();
      return;
    }
    fetch(`http://localhost:7099/truck/${licence}/locations`, {
      method: 'GET',
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
    }).then((response) => {
      return response.json();
    }).then((locations) => {
      updateMap(locations);
    }).catch((error) => {
      console.log(error);
      throw error;
    });
  };

  useEffect(() => {
    if (map === null) {
      const map = new window.google.maps.Map(document.getElementById('map'), {
        center: {lat: defaultLatitude, lng: defaultLongitude},
        zoom: 10,
        disableDefaultUI: true
      });
      setMap(map);
    }
  }, [map]);

  return (
    <div>
      <div className="Control-container">
        <input
          className="Licence-field"
          placeholder="Search by licence plate"
          value={licence}
          onChange={(event) => setLicence(event.target.value)}
          onKeyPress={(event) => {
            if (event.key === 'Enter') {
              getData();
            }
          }}
        />
        <select
          className="Poi-field"
          value={poi}
          onChange={(event) => setPoi(event.target.value)}
          onKeyPress={(event) => {
            if (event.key === 'Enter') {
              getData();
            }
          }}
        >
          {renderPoiOptions()}
        </select>
        <select
          className="Radius-field"
          value={radius}
          onChange={(event) => setRadius(event.target.value)}
          onKeyPress={(event) => {
            if (event.key === 'Enter') {
              getData();
            }
          }}
        >
          {radiusOptions.map((option) => renderOption(option.value, option.description))}
        </select>
        <button
          className="Apply-button"
          onClick={() => {getData();}}
        >
          Apply
        </button>
      </div>
      <div id="map" className="Map"/>
    </div>
  );
};

export default FleetManager;