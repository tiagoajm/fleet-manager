const poiType = {
  VIEW_ALL: {
    googleType: ['gas_station','restaurant','lodging'],
    description: 'View all'
  },
  GAS_STATION: {
    googleType: ['gas_station'],
    description: 'Gas Stations',
    image: `${process.env.PUBLIC_URL}/icn-gas-station.png`
  },
  RESTAURANTS: {
    googleType: ['restaurant'],
    description: 'Restaurants',
    image: `${process.env.PUBLIC_URL}/icn-restaurant.png`
  },
  HOTELS: {
    googleType: ['lodging'],
    description: 'Hotels',
    image: `${process.env.PUBLIC_URL}/icn-hotel.png`
  }
};

Object.freeze(poiType);

export default poiType;
