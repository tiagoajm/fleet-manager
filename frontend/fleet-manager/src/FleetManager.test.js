import React from 'react';
import ReactDOM from 'react-dom';
import { getMarkerImgFromGoogleType } from './FleetManager';

describe('Test getMarkerImgFromGoogleType', () => {
  const testData = [
    {
      googleType: 'gas_station',
      expectedImage: '/icn-gas-station.png',
    },
    {
      googleType: 'restaurant',
      expectedImage: '/icn-restaurant.png',
    },
    {
      googleType: 'lodging',
      expectedImage: '/icn-hotel.png',
    },
    {
      googleType: 'non-existing',
      expectedImage: undefined,
    },
  ];

  testData.forEach((testProperties) => {
    it(`Test getMarkerImgFromGoogleType for ${testProperties.googleType} google type`, () => {
      const result = getMarkerImgFromGoogleType(testProperties.googleType);
      expect(result).toEqual(testProperties.expectedImage);
    });
  });
});
