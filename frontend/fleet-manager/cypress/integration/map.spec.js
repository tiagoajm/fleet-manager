/// <reference types="Cypress" />

it('Check fields to select are present', () => {
  cy.visit('');
  cy.get('.Licence-field').should('be.visible');
  cy.get('.Poi-field').should('be.visible');
  cy.get('.Radius-field').should('be.visible');
  cy.get('.Apply-button').should('be.visible');
});

context('Check map action', () => {
  let polyfill;
  before(() => {
    cy.server();
    const polyfillUrl = 'https://unpkg.com/unfetch/dist/unfetch.umd.js';
    cy.request(polyfillUrl)
      .then((response) => {
        polyfill = response.body;
      });
    Cypress.Server.defaults({ force404: true });
      // Workaround for fetch to work with cypress stubs
      // Remove when fetch works properly with cypress routes
      cy.on('window:before:load', (win) => {
        delete win.fetch;
        win.eval(polyfill);
        win.fetch = win.unfetch;
      });
  });

  it('Check map path', () => {
    const currentLocationSelector = 'img[src="/icn-current-location.png"]';
    const pathLocationSelector = 'img[src="/icn-path.png"]';
    const firstLocationSelector = 'img[src="/icn-first-location.png"]';
    cy.fixture('locations.json').as('locations');
    cy.route('GET', '**/truck/THE_LICENCE/locations', 'fixture:locations.json', '@locations');
    cy.visit('');
    cy.get('.Licence-field').should('be.visible').type('THE_LICENCE');
    cy.get('.Apply-button').click();
    // images are being loaded twice, so the length will be the double
    cy.get(currentLocationSelector).should('have.length', 2);
    cy.get(currentLocationSelector).should('be.visible');
    cy.get(pathLocationSelector).should('have.length', 6);
    cy.get(pathLocationSelector).should('be.visible');
    cy.get(firstLocationSelector).should('have.length', 2);
    cy.get(firstLocationSelector).should('be.visible');
  });
});
