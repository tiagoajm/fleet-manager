create table truck_info
(
  licence_plate varchar(50),
  constraint truck_info_pk primary key (licence_plate)
);

create table truck_location
(
  licence_plate varchar(50),
  timestamp bigint not null,
  latitude numeric not null,
  longitude numeric not null,
  constraint truck_location_pk primary key(licence_plate, timestamp),
  constraint truck_location_fk foreign key(licence_plate) references truck_info(licence_plate)
);

-- some dummy values

insert into truck_info(licence_plate) values('LICENCE1');
insert into truck_info(licence_plate) values('LICENCE2');
insert into truck_info(licence_plate) values('LICENCE3');

insert into truck_location(licence_plate, timestamp, latitude, longitude) values('LICENCE1', 1, 38.717090, -9.149215);
insert into truck_location(licence_plate, timestamp, latitude, longitude) values('LICENCE1', 2, 38.717770, -9.151543);
insert into truck_location(licence_plate, timestamp, latitude, longitude) values('LICENCE1', 3, 38.718389, -9.152429);
insert into truck_location(licence_plate, timestamp, latitude, longitude) values('LICENCE1', 4, 38.719971, -9.154215);
insert into truck_location(licence_plate, timestamp, latitude, longitude) values('LICENCE1', 5, 38.722143, -9.152075);
insert into truck_location(licence_plate, timestamp, latitude, longitude) values('LICENCE2', 1, 38.717090, -9.149215);
insert into truck_location(licence_plate, timestamp, latitude, longitude) values('LICENCE3', 1, 52.538393, 13.232646);