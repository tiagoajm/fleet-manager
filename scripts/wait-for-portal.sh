#!/bin/bash

set -uo

readonly RED='\033[0;31m'
readonly GREEN='\033[0;32m'
readonly NC='\033[0m' # No Color

print_success() {
  echo -e " --> ${GREEN}SUCCESS!${NC}"
}

print_failure() { 
  echo -e " --> ${RED}FAILED${NC}"
}

run_curl() {
  local iteration=1
  local max_retries=60
  while [[ ${iteration} -lt ${max_retries} ]]; do
    echo -n "Try to perform GET on http://localhost:5000"
	response_code=$(curl -s -o /dev/null -w "%{http_code}" --insecure http://localhost:5000)
	if [[ ${response_code} -eq 200 ]]; then
	  print_success && break
	fi
    print_failure
    sleep 5
    iteration=$((iteration+1))
  done
  if [[ ${iteration} -eq ${max_retries} ]]; then
    exit 1
  fi
}

run_curl