# MAN truck monitor

Welcome to MAN truck monitor.

This project have several components:
- The frontend was developed with react
- The restful webservices were developed in JAVA with spring-boot
- The database used is Postgres database

Beside the components other technologies were used:
- Docker and docker compose were used
- Openapi was used for the restful webservices documentation
- Google Maps API was used to provide the map
- Gitlab was used to CI (continuous integration)

## Run application
To make it easier to run the application, all the components were previously compiled.

To run the application you need to have docker installed and internet access.

### Steps
- Install docker (available [here](https://docs.docker.com/install/))
- Go to __docker-compose__ folder
- Run docker compose  
```$ docker-compose -f docker-compose.yml up```

After docker compose is up you can access the page by going to http://localhost:5000

*Note:* to properly run the application you need to have the ports __5000__ (portal), __7099__ (webservices) and __5432__ (database) available.

## Compile

### Database

The database used was Postgres and will be running with docker.  
The SQL script is under __docker-images/postgres/sql__ folder.  
You can build and run Postgres docker with the following commands:  
```
$ docker build -t fleet-manager-postgres:1.0.0 .  
$ docker run -it -p 5432:5432 fleet-manager-postgres:1.0.0 
```
The data base will be available at localhost:5432

### Services

To compile the services you will need the following:
- Maven installed (available [here](https://maven.apache.org/download.cgi))
- JDK 1.8 installed (available [here](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html))

To compile the project go to __services-api__ folder and run the command:  
````$ mvn clean compile````

To create a __jar__ run the command:  
````$ mvn clean package````

### Portal

To compile the portal you will need the following:
- Install Node.js (available [here](https://nodejs.org/en/download/))
- Install yarn Globally (using npm):  
```$ npm install -g yarn```
- Go to __fronted/fleet-manager__ folder
- Download all the required packages (node modules):  
```$ yarn install```

If you want to run locally execute the command:  
````$ yarn start````  
The portal will be available at http://localhost:3000

If you want to bundle the portal to production run the command:
````$ yarn build````

## API documentation

The API documentation was made using __openapi__.  
You can find the API documentation under __documentation__ folder.

## Tests

### Services

There is a set of unit tests for the services.  
To run the tests go to __services-api__ folder and run the command:  
````$ mvn clean test````

### Portal

#### Unit tests

To run portal unit tests go to __frontend/fleet-manager__ folder and run the command:  
````$ yarn test````

#### Cypress tests

To run cypress tests go to __frontend/fleet_manager__ folder and run the following commands:  
````
$ yarn install  
$ yarn cypress open
````

## Continuous integration

The project contains CI (continuous integration) running in gitlab.
You can find the file in the root folder with the name __.gitlab-ci.yaml__  
