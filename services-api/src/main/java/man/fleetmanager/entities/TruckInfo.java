package man.fleetmanager.entities;

public class TruckInfo {
    private String licencePlate;

    public TruckInfo() {}

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(final String licencePlate) {
        this.licencePlate = licencePlate;
    }
}
