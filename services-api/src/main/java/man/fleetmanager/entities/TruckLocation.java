package man.fleetmanager.entities;

public class TruckLocation {
    private String licence;
    private Long timestamp;
    private Double latitude;
    private Double longitude;

    public String getLicence() {
        return licence;
    }

    public void setLicence(final String licence) {
        this.licence = licence;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final Long timestamp) {
        this.timestamp = timestamp;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(final Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(final Double longitude) {
        this.longitude = longitude;
    }
}
