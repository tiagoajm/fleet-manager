package man.fleetmanager.dataaccess;

import man.fleetmanager.datamapper.TruckInfoMapper;
import man.fleetmanager.datamapper.TruckLocationMapper;
import man.fleetmanager.entities.TruckInfo;
import man.fleetmanager.entities.TruckLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TruckDataAccess {
    @Autowired
    @Qualifier("postgresNamedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate postgresJdbcTemplate;

    private final static String TRUCK_LOCATIONS_QUERY =
        "SELECT licence_plate, timestamp, latitude, longitude FROM truck_location " +
        "WHERE upper(licence_plate) = :licence_plate " +
        "ORDER BY timestamp desc";

    private final static String TRUCK_INFO_QUERY =
        "SELECT licence_plate FROM truck_info " +
        "WHERE upper(licence_plate) = :licence_plate";

    private final static String CREATE_TRUCK_INFO_QUERY =
        "INSERT INTO truck_info (licence_plate) " +
        "VALUES (:licence_plate)";

    private final static String CREATE_TRUCK_LOCATION_QUERY =
        "INSERT INTO truck_location (licence_plate, timestamp, latitude, longitude) " +
        "VALUES (:licence_plate, :timestamp, :latitude, :longitude)";

    public List<TruckLocation> getTruckLocations(final String licencePlate) {
        final MapSqlParameterSource queryParams = new MapSqlParameterSource();
        queryParams.addValue("licence_plate", licencePlate.toUpperCase());
        List<TruckLocation> locations = postgresJdbcTemplate.query(TRUCK_LOCATIONS_QUERY, queryParams, new TruckLocationMapper());
        return locations;
    }

    public TruckInfo getTruckInfo(final String licencePlate) {
        final MapSqlParameterSource queryParams = new MapSqlParameterSource();
        queryParams.addValue("licence_plate", licencePlate.toUpperCase());
        List<TruckInfo> truckInfo = postgresJdbcTemplate.query(TRUCK_INFO_QUERY, queryParams, new TruckInfoMapper());
        if (!truckInfo.isEmpty()) {
            return truckInfo.get(0);
        }
        return null;
    }

    public void createTruckInfo(final TruckInfo truckInfo) {
        final MapSqlParameterSource queryParams = new MapSqlParameterSource();
        queryParams.addValue("licence_plate", truckInfo.getLicencePlate().toUpperCase());
        postgresJdbcTemplate.update(CREATE_TRUCK_INFO_QUERY, queryParams);
    }

    private long getTimestampToInsert(final Long timestamp) {
        return timestamp == null || timestamp == 0 ? System.currentTimeMillis(): timestamp;
    }

    public void createTruckLocation(final TruckLocation truckLocation) {
        final MapSqlParameterSource queryParams = new MapSqlParameterSource();
        queryParams.addValue("licence_plate", truckLocation.getLicence().toUpperCase());
        queryParams.addValue("timestamp", getTimestampToInsert(truckLocation.getTimestamp()));
        queryParams.addValue("latitude", truckLocation.getLatitude());
        queryParams.addValue("longitude", truckLocation.getLongitude());
        postgresJdbcTemplate.update(CREATE_TRUCK_LOCATION_QUERY, queryParams);
    }

}
