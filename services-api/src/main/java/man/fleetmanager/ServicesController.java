package man.fleetmanager;

import man.fleetmanager.business.TruckBusiness;
import man.fleetmanager.dataaccess.TruckDataAccess;
import man.fleetmanager.entities.TruckInfo;
import man.fleetmanager.entities.TruckLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class ServicesController {

    @Autowired
    private TruckDataAccess dataAccess;

    @Autowired
    private TruckBusiness business;

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public String welcome() {
        return "welcome to fleet-manager";
    }

    @RequestMapping(path = "/truck/{licence}/locations", method = RequestMethod.GET)
    public ResponseEntity<List<TruckLocation>> getTruckLocation(@PathVariable("licence") final String licence) {
        return business.getTruckLocations(licence);
    }

    @RequestMapping(path = "/truck/{licence}/location", method = RequestMethod.POST)
    public ResponseEntity addLocation(@PathVariable("licence") final String licencePlate, @RequestBody final TruckLocation location) {
        return business.insertLocation(licencePlate, location);
    }

    @RequestMapping(path = "/truck/{licence}", method = RequestMethod.GET)
    public ResponseEntity<TruckInfo> getTruckInfo(@PathVariable("licence") final String licencePlate) {
        return business.getTruckInfo(licencePlate);
    }

    @RequestMapping(path = "/truck", method = RequestMethod.POST)
    public ResponseEntity createTruckInfo(@RequestBody final TruckInfo truckInfo) {
        return business.insertTruckInfo(truckInfo);
    }
}
