package man.fleetmanager.datamapper;

import man.fleetmanager.entities.TruckInfo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TruckInfoMapper implements RowMapper {
    @Override
    public Object mapRow(final ResultSet resultSet, final int i) throws SQLException {
        final TruckInfo truckInfo = new TruckInfo();
        truckInfo.setLicencePlate(resultSet.getString("licence_plate"));
        return truckInfo;
    }
}
