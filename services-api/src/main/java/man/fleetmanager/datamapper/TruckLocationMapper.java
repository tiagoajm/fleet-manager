package man.fleetmanager.datamapper;

import man.fleetmanager.entities.TruckLocation;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TruckLocationMapper implements RowMapper {

    @Override
    public TruckLocation mapRow(final ResultSet resultSet, final int i) throws SQLException {
        final TruckLocation location = new TruckLocation();
        location.setLicence(resultSet.getString("licence_plate"));
        location.setTimestamp(resultSet.getLong("timestamp"));
        location.setLatitude(resultSet.getDouble("latitude"));
        location.setLongitude(resultSet.getDouble("longitude"));
        return location;
    }

}
