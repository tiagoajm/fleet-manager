package man.fleetmanager.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration
public class PostgresDataSourceConfig {
    @Bean(name = "tmPostgres")
    @Autowired
    DataSourceTransactionManager tmPostgres(@Qualifier("postgresDataSource") final DataSource datasource) {
        return new DataSourceTransactionManager(datasource);
    }

    @Bean(name = "postgresDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.postgres")
    public DataSource postgresDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "postgresJdbcTemplate")
    public JdbcTemplate postgresJdbcTemplate(@Qualifier("postgresDataSource") final DataSource dsPostgres) {
        return new JdbcTemplate(dsPostgres);
    }

    @Bean(name = "postgresNamedParameterJdbcTemplate")
    public NamedParameterJdbcTemplate postgresNamedParameterJdbcTemplate(@Qualifier("postgresDataSource") final DataSource dsPostgres) {
        return new NamedParameterJdbcTemplate(dsPostgres);
    }

}
