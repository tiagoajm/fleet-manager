package man.fleetmanager.business;

import man.fleetmanager.dataaccess.TruckDataAccess;
import man.fleetmanager.entities.TruckInfo;
import man.fleetmanager.entities.TruckLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class TruckBusiness {
    @Autowired
    private TruckDataAccess dataAccess;

    /**
     * Get all locations known locations for the truck with the licence plate @licencePlate
     */
    @Transactional(value = "tmPostgres")
    public ResponseEntity<List<TruckLocation>> getTruckLocations(final String licencePlate) {
       return ResponseEntity.ok(dataAccess.getTruckLocations(licencePlate));
    }

    protected boolean isLocationLicenceValid(final String requestLicence, final String locationLicence) {
        if (locationLicence != null && !requestLicence.toUpperCase().equals(locationLicence.toUpperCase())) {
            return false;
        }
        return true;
    }

    protected boolean isLatLngFilled(final Double latitude, final Double longitude) {
        if (latitude == null || longitude == null) {
            return false;
        }
        return true;
    }

    /**
     * Insert a location to an existing truck
     */
    @Transactional(value = "tmPostgres")
    public ResponseEntity insertLocation(final String licencePlate, final TruckLocation truckLocation) {
        if (!isLicencePlateFilled(licencePlate)) {
            return ResponseEntity.badRequest().body("Invalid licence plate");
        }
        if (!isLocationLicenceValid(licencePlate, truckLocation.getLicence())) {
            return ResponseEntity.badRequest().body("The requested licence is not the same present in the location info body");
        }
        if (!isLatLngFilled(truckLocation.getLatitude(), truckLocation.getLongitude())) {
            return ResponseEntity.badRequest().body("Latitude and longitude are required");
        }
        TruckInfo truckInfo = dataAccess.getTruckInfo(licencePlate);
        if (truckInfo == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        truckLocation.setLicence(licencePlate);
        dataAccess.createTruckLocation(truckLocation);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    protected TruckInfo getTruckInfoData(final String licencePlate) {
        return dataAccess.getTruckInfo(licencePlate);
    }

    protected boolean isLicencePlateFilled(final String licencePlate) {
        if (licencePlate == null || licencePlate.isEmpty()) {
            return false;
        }
        return true;
    }

    protected boolean licencePlateExists(final String licencePlate) {
        if (getTruckInfoData(licencePlate) == null) {
            return false;
        }
        return true;
    }

    /**
     * Insert a new truck information
     */
    @Transactional(value = "tmPostgres")
    public ResponseEntity insertTruckInfo(final TruckInfo truckInfo) {
        if (!isLicencePlateFilled(truckInfo.getLicencePlate())) {
            return ResponseEntity.badRequest().body("Invalid licence plate");
        }
        if (licencePlateExists(truckInfo.getLicencePlate())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Licence place is duplicated");
        }
        dataAccess.createTruckInfo(truckInfo);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     * Get the truck info with the given licence plate @licencePlate
     */
    @Transactional(value = "tmPostgres")
    public ResponseEntity getTruckInfo(final String licencePlate) {
        if (!isLicencePlateFilled(licencePlate)) {
            return ResponseEntity.badRequest().body("Invalid licence plate");
        }
        TruckInfo truckInfo = getTruckInfoData(licencePlate);
        if (truckInfo == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok(truckInfo);
    }
}
