package man.fleetmanager.business;

import man.fleetmanager.entities.TruckInfo;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TruckBusinessTest {

    private TruckBusiness business;

    @BeforeTest
    public void init() {
        business = new TruckBusiness();
    }

    @DataProvider(name = "TestIsLocationLicenceValidDataProvider")
    public Object[][] getTestIsLocationLicenceValidDataProvider() {
        return new Object[][] {
            {"abc", "abc", true},
            {"xyz", null, true},
            {"ABC", "abc", true},
            {"", "abc", false},
            {"abc", "xyz", false},
        };
    }

    @Test(dataProvider = "TestIsLocationLicenceValidDataProvider")
    public void testIsLocationLicenceValid(final String requestLicence, final String locationLicence, final boolean expectedResult) {
        Assert.assertEquals(business.isLocationLicenceValid(requestLicence, locationLicence), expectedResult);
    }

    @DataProvider(name = "IsLatLngFilledDataProvider")
    public Object[][] getIsLatLngFilledDataProvider() {
        return new Object[][] {
            {null, null, false},
            {null, 123.5, false},
            {4323.21, null, false},
            {9923.12, 9492.124, true},
        };
    }

    @Test(dataProvider = "IsLatLngFilledDataProvider")
    public void testIsLatLngFilled(final Double latitude, final Double longitude, final boolean expectedResult) {
        Assert.assertEquals(business.isLatLngFilled(latitude, longitude), expectedResult);
    }

    @DataProvider(name = "IsLicencePlateFilledDataProvider")
    public Object[][] getIsLicencePlateFilledDataProvider() {
        return new Object[][] {
            {null, false},
            {"", false},
            {"abc", true},
        };
    }

    @Test(dataProvider = "IsLicencePlateFilledDataProvider")
    public void testIsLicencePlateFilled(final String licencePlate, final boolean expectedResult) {
        Assert.assertEquals(business.isLicencePlateFilled(licencePlate), expectedResult);
    }

    @DataProvider(name = "LicencePlateExistsDataProvider")
    public Object[] getLicencePlateExistsDataProvider() {
        return new Object[] {
            true,
            false
        };
    }

    @Test(dataProvider = "LicencePlateExistsDataProvider")
    public void testLicencePlateExists(final boolean expectedResult) {
        final String licencePlate = "LICENCE_PLATE";
        TruckInfo truckInfo = null;
        if (expectedResult) {
            truckInfo = new TruckInfo();
        }
        TruckBusiness businessTruck = Mockito.spy(TruckBusiness.class);
        Mockito.doReturn(truckInfo).when(businessTruck).getTruckInfoData(licencePlate);
        Assert.assertEquals(businessTruck.licencePlateExists(licencePlate), expectedResult);
    }
}
